package com.tll.mybatisplusstudy;

import com.tll.mybatisplusstudy.entity.User;
import com.tll.mybatisplusstudy.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 16:22
 */

@SpringBootTest
public class ServiceTest {

    @Autowired
    private UserService userService;

    //测试总记录数
    @Test
    void testCount(){
        long count = userService.count();
        System.out.println("user表总记录数："+count);
    }

    @Test
    void testSaveBatch(){

        ArrayList<User> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setAge(i);
            user.setEmail(i+"@163.com");
            user.setName(i+"name");
            list.add(user);
        }
        boolean flag = userService.saveBatch(list);
        System.out.println("是否插入成功"+flag);
    }
}
