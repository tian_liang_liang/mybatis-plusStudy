package com.tll.mybatisplusstudy;

import com.tll.mybatisplusstudy.entity.User;
import com.tll.mybatisplusstudy.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class MybatisPlusStudyApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    void getAllUser() {

        userMapper.selectList(null).forEach(System.out::println);
    }

    //插入
    @Test
    void testInsertUser() {
        User user = new User(null, "tll", 23, "2289091393@qq.com",0,1);
        int row = userMapper.insert(user);
        System.out.println("影响行数");
        System.out.println("生成Id" + user.getId());
    }

    //根据id进行删除
    @Test
    void testDeleterById() {
        int row = userMapper.deleteById(1L);
        System.out.println("影响行数： " + row);
    }

    //通过Id批量删除数据
    @Test
    void testDeleteBatchIds() {
        List<Long> list = Arrays.asList(1L, 2L, 3L);
        int row = userMapper.deleteBatchIds(list);
        System.out.println("影响行数： " + row);
    }

    //通过map中所设置的条件进行删除
    @Test
    void testDeleteByMap(){
        Map<String, Object> map = new HashMap<>();
        map.put("age",20);
        map.put("name","Jack");
        int row = userMapper.deleteByMap(map);
        System.out.println("影响行数： " + row);

    }

    //修改信息
    @Test
    void testUpdateById(){
        User user = new User(4L, "花生", 1, "huas@163.com",0,1);
        int row = userMapper.updateById(user);
        System.out.println("影响行数： " + row);

    }

    //通过Id查询
    @Test
    void testSelectById(){
        User user = userMapper.selectById(5L);
        System.out.println(user);

    }

    //根据多个Id查询多个用户信息
    @Test
    void testSelectBatchIds(){
        List<Long> list = Arrays.asList(4L, 5L);
        List<User> users = userMapper.selectBatchIds(list);
        users.forEach(System.out::println);

    }

    //通过map进行查询
    @Test
    void testSelectByMap(){
        Map<String, Object> map = new HashMap<>();
        map.put("name","Tom");
        map.put("age",11);
        List<User> list = userMapper.selectByMap(map);
        list.forEach(System.out::println);
    }

    //查询所有信息
    @Test
    void testSelectAll(){
        List<User> list = userMapper.selectList(null);
        list.forEach(System.out::println);
    }
}
