package com.tll.mybatisplusstudy;

import com.tll.mybatisplusstudy.entity.User;
import com.tll.mybatisplusstudy.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 18:06
 */
@SpringBootTest
public class VersionTest {

    @Autowired
    private UserService userService;

    @Test
    void testVersion() {
        //同时获取id=1L 的用户
        User user1 = userService.getById(1L);
        User user2 = userService.getById(1L);

        //修改user1/user2
        user1.setEmail("第一次修改");
        user2.setEmail("diyicixiugai");

        //更新user1/user2
        boolean b = userService.updateById(user2);
        System.out.println(b);
        boolean flag = userService.updateById(user1);
        if (!flag){
            user1 = userService.getById(1L);
            user1.setEmail("第一次修改");
            userService.updateById(user1);
        }
        User user3 = userService.getById(1L);
        System.out.println(user3.getEmail());
    }
}
