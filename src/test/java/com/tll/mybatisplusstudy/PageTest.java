package com.tll.mybatisplusstudy;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tll.mybatisplusstudy.entity.User;
import com.tll.mybatisplusstudy.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 17:55
 */
@SpringBootTest
public class PageTest {

    @Autowired
    private UserService userService;

    @Test
    void testPage(){
        Page<User> page = new Page<>(1,3);
        userService.page(page);
        List<User> records = page.getRecords();
        long size = page.getSize();
        boolean hasNext = page.hasNext();
        long total = page.getTotal();
        System.out.println("是否有下一页"+hasNext);
        System.out.println("每页获取数"+size);
        System.out.println("总记录数"+total);
        records.forEach(System.out::println);
    }
}
