package com.tll.mybatisplusstudy;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 19:49
 */
public class FastAutoGeneratorTest {

    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://139.9.138.30:3306/mybatisstudy?serverTimezone=GMT%2B8&characterEncoding=utf-8&useSSL=false", "root", "123456")
                .globalConfig(builder -> {
                    builder.author("tll") // 设置作者
                            //.enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D:\\IDEACodes\\mybatis-plusStudy\\src\\main\\java\\com\\tll\\mybatisplusstudy"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("") // 设置父包名
                            .moduleName("") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D:\\IDEACodes\\mybatis-plusStudy\\src\\main\\java\\com\\tll\\mybatisplusstudy\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("t_teacher")// 设置需要生成的表名
                            .addTablePrefix("t_", "c_")
                            .entityBuilder()
                            .enableLombok(); // 设置过滤表前缀

                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

//        FastAutoGenerator.create("jdbc:mysql://139.9.138.30:3306/mybatisstudy?serverTimezone=GMT%2B8&characterEncoding=utf-8&useSSL=false", "root", "123456")
//                .globalConfig(builder -> {
//                    builder.author("tll")
//                            // 设置作者
//                            .enableSwagger()
//                            // 开启 swagger 模式
//                            .fileOverride()
//                            // 覆盖已生成文件
//                            .outputDir("D:\\IDEACodes\\mybatis-plusStudy");
//                    // 指定输出目录
//                }).packageConfig(builder -> {
//            builder.parent("com.tll")
//                    // 设置父包名
//                    .moduleName("mybatisplusstudy")
//                    // 设置父包模块名
//                    .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D:\\IDEACodes\\mybatis-plusStudy"));
//            // 设置mapperXml生成路径
//        }).strategyConfig(builder -> {
//            builder.addInclude("t_teacher")
//                    // 设置需要生成的表名
//                    .addTablePrefix("t_", "c_");
//            // 设置过滤表前缀
//        }).templateEngine(new FreemarkerTemplateEngine())
//
//                // 使用Freemarker 引擎模板，默认的是Velocity引擎模板
//                .execute();
    }
}

