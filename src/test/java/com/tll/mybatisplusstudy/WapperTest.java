package com.tll.mybatisplusstudy;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tll.mybatisplusstudy.entity.User;
import com.tll.mybatisplusstudy.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 17:09
 * Wapper测试
 */
@SpringBootTest
public class WapperTest {

    @Autowired
    private UserMapper userMapper;

    //组装查询条件
    @Test
    void test01() {
        //查询用户名包含a，年龄在20到30之间，并且邮箱不为null的用户信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //用户名包含a
        queryWrapper.like("name", "a");
        //年龄在20~30之间
        queryWrapper.between("age", 20, 30);
        queryWrapper.isNotNull("email");
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);
    }

    //组装排序条件
    //ASC:升序  DESC：降序
    @Test
    void test02() {
        //按年龄降序查询用户，如果年龄相同则按id升序排列
        //SELECT id,name,age,email,is_deleted FROM t_user WHERE is_deleted=0 ORDER BY age DESC,id ASC
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("age").orderByDesc("id");
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);
    }


    //组装删除条件
    @Test
    void test03() {
        //删除email为空的用户
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNull("email");
        int row = userMapper.delete(queryWrapper);
        System.out.println("影响行数 " + row);
    }

    //条件的优先级
    @Test
    void test04() {

        //将（年龄大于20并且用户名中包含有a）或邮箱为null的用户信息修改
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge("age", 20)
                    .like("name", "a")
                    .or()
                    .isNull("email");
        User user = new User();
        user.setName("haha");
        user.setAge(23);
        user.setEmail("hah@163.com");
        int row = userMapper.update(user, queryWrapper);
        System.out.println("影响行数"+row);

    }

    //组装select之句
    @Test
    void test05(){
        //查询用户信息的username和age字段
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("name","age");
        //selectMaps()返回Map集合列表，通常配合select()使用，避免User对象中没有被查询到的列值 为null
        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        maps.forEach(System.out::println);

    }

    //实现子查询
    @Test
    void test06(){
        //查询id小于等于3的用户信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.inSql("id","select id from t_user where id <= 3");
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);

    }

}
