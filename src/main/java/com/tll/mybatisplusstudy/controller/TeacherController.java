package controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tll
 * @since 2022-03-20
 */
@Controller
@RequestMapping("/teacher")
public class TeacherController {

}
