package entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author tll
 * @since 2022-03-20
 */
@Getter
@Setter
@TableName("t_teacher")
public class Teacher implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 讲师Id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 讲师名称
     */
    private String username;

    /**
     * 讲师密码
     */
    private String password;


}
