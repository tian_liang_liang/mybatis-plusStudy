package com.tll.mybatisplusstudy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tll.mybatisplusstudy.entity.User;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 15:32
 */
public interface UserService extends IService<User> {
}
