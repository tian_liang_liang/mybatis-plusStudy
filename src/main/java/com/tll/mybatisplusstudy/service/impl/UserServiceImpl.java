package com.tll.mybatisplusstudy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tll.mybatisplusstudy.entity.User;
import com.tll.mybatisplusstudy.mapper.UserMapper;
import com.tll.mybatisplusstudy.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 16:24
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
