package service;

import entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tll
 * @since 2022-03-20
 */
public interface ITeacherService extends IService<Teacher> {

}
