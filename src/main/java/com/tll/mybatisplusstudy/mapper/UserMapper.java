package com.tll.mybatisplusstudy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tll.mybatisplusstudy.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author tll
 * @version 1.0.0
 * @date 2022/3/20 15:32
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
