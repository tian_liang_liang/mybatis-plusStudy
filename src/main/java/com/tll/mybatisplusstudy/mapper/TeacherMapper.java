package mapper;

import entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tll
 * @since 2022-03-20
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

}
